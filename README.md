# duktape-ohos 
#### 项目介绍

- 项目名称：duktape-ohos
- 所属系列：openharmony的第三方组件适配移植
- 功能：用于Duktape嵌入式JavaScript引擎
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release 1.3.0

#### 演示效果
![](img/demo.gif)

#### 安装教程

1.在项目根目录下的 `build.gradle` 文件中，

 ```
// 添加maven仓库
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/release/'
    }
}
 ```
2.在entry模块的 `build.gradle` 文件中，
 ```
// 添加依赖库
dependencies {
    implementation 'com.gitee.chinasoft_ohos:duktape:1.0.0'
    implementation 'com.gitee.chinasoft_ohos:quickjs:1.0.0'
}

 ```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

> 使用

```java
Duktape duktape = Duktape.create();
try {
  HiLog.info(TAG, duktape.evaluate("'hello world'.toUpperCase();").toString());
} finally {
  duktape.close();
}
```

> 当前与JavaScript接口时支持以下Java类型：

- `boolean` 和 `Boolean`
- `int`和`Integer`-如从Java调用JavaScript的一种论点只（不返回值）。
- `double` 和 `Double`
- `String`
- `void` -作为返回值。

`Object`声明中也受支持，但传递的实际值的类型必须是上述类型之一或为null。

> 从JavaScript调用Java

从JavaScript调用Java，你可以提供一个Java对象作为JavaScript全局变量使用。 

> 例子

假设我们想要在JavaScript中公开okio的ByteString的功能，将十六进制编码的字符串转换回UTF-8。首先，定义一个Java接口来声明你想从JavaScript调用的方法:

```java
interface Utf8 {
  String fromHex(String hex);
}
```

接下来，在Java代码中实现接口(我们把繁重的工作留给okio):

```java
Utf8 utf8 = new Utf8() {
  @Override 
  public String fromHex(String hex) {
    return okio.ByteString.decodeHex(hex).utf8();
  }
};
```

现在你可以将对象设置为JavaScript全局对象，使其在JavaScript代码中可用:

```java
// Attach our interface to a JavaScript object called Utf8.
duktape.set("Utf8", Utf8.class, utf8);

String greeting = (String) duktape.evaluate(""
    // Here we have a hex encoded string.
    + "var hexEnc = 'EC9588EB8595ED9598EC84B8EC9A9421';\n"
    // Call out to Java to decode it!
    + "var message = Utf8.fromHex(hexEnc);\n"
    + "message;");

HiLog.info(TAG, greeting);
```

> 从Java调用JavaScript

您可以将Java接口附加到JavaScript全局对象，并直接从Java调用JavaScript函数!函数参数和返回值支持相同的Java类型。

> 例子

想象一个我们没有okio的ByteString的世界。幸运的是，有一种Duktape允许我们将十六进制编码的字符串转换回UTF-8格式!我们可以很容易地建立一个允许我们直接从Java代码中使用它的代理。首先，定义一个Java接口它声明了你想调用的JavaScript方法:

```java
interface Utf8 {
  String fromHex(String hex);
}
```

接下来，我们在Duktape中定义一个全局JavaScript对象来连接:

```java
// Note that Duktape.dec returns a Buffer, we must convert it to a String return value.
duktape.evaluate(""
    + "var Utf8 = {\n"
    + "  fromHex: function(v) { return String(Duktape.dec('hex', v)); }\n"
    + "};");
```

现在你可以把我们的接口连接到JavaScript全局代码，使它在Java代码中可用:

```java
// Connect our interface to a JavaScript object called Utf8.
Utf8 utf8 = duktape.get("Utf8", Utf8.class);

// Call into the JavaScript object to decode a string.
String greeting = utf8.fromHex("EC9588EB8595ED9598EC84B8EC9A9421");
HiLog.info(TAG, greeting);
```

#### 测试信息

CodeCheck代码测试无异常  

CloudTest代码测试无异常  

病毒安全检测通过  

当前版本demo功能与原组件基本无差异  

#### 版本迭代

- 1.0.0  

#### 版权和许可信息  

```
Copyright 2015 Square, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
Note: The included C code from Duktape is licensed under MIT.
